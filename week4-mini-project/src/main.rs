use actix_web::{get, App, HttpServer, Responder};
use rand::Rng;

#[get("/")]
async fn random_number() -> impl Responder {
    // Generate a random number between 1 and 10
    let random_number = rand::thread_rng().gen_range(1..=10);
    format!("Random number from 1 to 10: {}.\nRefresh to get a new one!", random_number)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(random_number)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
