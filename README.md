# IDS 721 Mini Project 4
This project is to Containerize a Rust Actix Web Service. In my case, I built a single application using Rust and Actix to generate a random number from 1 to 10 everytime you refresh the page.

## Packages and Dependencies
To install docker: Go to the docker official website and follow the directions to download and install docker.

## Build and deploy the project:
- Run `cargo new week4-mini-project` to create a blank project.
- Update the code in `/src/main.rs` file to add functionalities to the rust function. 
- Add dependencies to `Cargo.toml` file. I included `actix-web = "4" rand = "0.8"`.
- Run `cargo run` to test whether the main function works locally. 
- Open up the docker console and make sure that the docker desktop is running.
- Write the dockerfile to specify the steps including `FROM, WORKDIR, USER, COPY, RUN, EXPOSE, and CMD`.
- Build the docker image by running `docker build -t week4-mini-project .`
- Run the docker container by running `docker run -p 8080:8080 week4-mini-project`

## Screenshots
- Screenshot of Docker building function running: 
![Alt text](/screenshots/docker_build.png)
- Screenshot of Docker is running locally:
![Alt text](/screenshots/docker_running.png)
- Screenshot of Docker console, which contains the week4-mini-project:
![Alt text](/screenshots/docker_console.png)
- Screenshot of the running web application:
![Alt text](/screenshots/example.png)
